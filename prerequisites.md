# Python
**Aixt** projects needs _Python 3.x_, which can be download from its [Home page](https://www.python.org/downloads/).

_NOTE:_ In addition, you have to add _Python_ to PATH.

## Modules

**Aixt** projects uses _sly_ Python's module for doing the lexer and syntactic analysis, _re_ for regular expresions analysis, and _yaml_ for the configuration files. You can install all the needed modules as follows: 


```
pip install sly re pyyaml
```
## Native C compilers
- **XC8**: Microchip and Atmel 8-bit microcontrollers
- **XC16**: Microchip 16-bit microcontrollers
- **ImageCraft**: Cypress PSoC1
- **nbc**: NXC compiler (LEGO Mindstorms NXT intelligent brick)
  
etc.
